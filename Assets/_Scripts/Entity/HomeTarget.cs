﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomeTarget : MonoBehaviour
{
    public GameObject PanelObject;

    public SceneName NextScene = SceneName.Undefined;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        PanelObject.GetComponent<LoadLevelScript>().PlayShowAnimation(OnShowAnimationFinished);
    }

    public void OnShowAnimationFinished()
    {
        if (NextScene != SceneName.Undefined)
        {
            SceneManagementService.LoadScene(NextScene);
        }
        else
        {
            Debug.LogWarning("No next scene defined");
        }
    }

}
