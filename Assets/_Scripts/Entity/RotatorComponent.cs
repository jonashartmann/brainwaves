﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotatorComponent : MonoBehaviour
{
    public Vector3 RotationRate;

    void FixedUpdate()
    {
        Quaternion NewRotation = new Quaternion();
        NewRotation.eulerAngles = transform.rotation.eulerAngles + RotationRate * Time.fixedDeltaTime;
        transform.rotation = NewRotation;
    }
}
