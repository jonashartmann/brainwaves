﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Kills the player upon collision.
 */
public class Blackhole : MonoBehaviour
{

    private void OnTriggerEnter2D(Collider2D Collider)
    {
        PlayerController Player = Collider.GetComponent<PlayerController>();
        if(Player != null)
        {
            Player.OnPlayerDeath();
        }
    }

}
