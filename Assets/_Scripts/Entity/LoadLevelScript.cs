﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadLevelScript : MonoBehaviour
{
    public delegate void OnFinishShowAnimation();
    private OnFinishShowAnimation _Callback;

    public string ShowAnimationName;
    public string HideAnimationName;

    private void Start()
    {
        PlayHideAnimation();
    }

    public void PlayShowAnimation(OnFinishShowAnimation Callback)
    {
        GetComponent<Animation>().Play(ShowAnimationName);
        _Callback = Callback;
    }

    public void PlayHideAnimation()
    {
        GetComponent<Animation>().Play(HideAnimationName);
    }

    private void OnFinishAnimation()
    {
        if(_Callback != null)
        {
            _Callback.Invoke();
            _Callback = null;
        }
    }
}
