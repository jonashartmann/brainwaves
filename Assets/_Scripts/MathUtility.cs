﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class MathUtility
{
    public const float SMALL_NUMBER = 0.000001f;
    public const float KINDA_SMALL_NUMBER = 0.0001f;
    
    public static T Max<T>(T a, T b) 
    {
        return Comparer<T>.Default.Compare(a, b) > 0 ? a : b;
    }

    public static T Min<T>(T a, T b)
    {
        return Comparer<T>.Default.Compare(a, b) < 0 ? a : b;
    }

    public static float Square(float x)
    {
        return x * x;
    }

    public static bool NearlyEqual(float a, float b)
    {
        const float SMALL = 0.01f;
        return Mathf.Abs(a - b) < SMALL;
    }

    public static T Clamp<T>(T x, T min, T max) where T : System.IComparable<T>
    {
        return x.CompareTo(min) < 0 ? min : x.CompareTo(max) < 0 ? x : max;  
    }

    public static Vector2 VLerp(Vector2 Current, Vector2 Target, float Alpha)
    {
        return new Vector2(
            Mathf.Lerp(Current.x, Target.x, Alpha),
            Mathf.Lerp(Current.y, Target.y, Alpha)
            );
    }

}
