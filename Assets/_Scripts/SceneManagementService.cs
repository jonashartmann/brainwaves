﻿using System.Collections;
using System.Collections.Generic;
using Sound;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManagementService : MonoBehaviour
{
	public static SceneManagementService Instance { get; private set; }

	private void Awake()
	{
		Instance = this;

		DontDestroyOnLoad(this.gameObject);
	}

	public static void LoadScene(SceneName sceneName)
	{
		AdjustMusic(sceneName);
		SceneManager.LoadSceneAsync((int) sceneName, LoadSceneMode.Single);
	}

	private static void AdjustMusic(SceneName sceneName)
	{
		if (sceneName == SceneName.FinalBoss)
		{
			if (MusicService.Instance != null) MusicService.Instance.PlayBossMusic();
		}
	}
}
