﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Sound
{
	public class MusicService : MonoBehaviour
	{
		private readonly List<AudioSource> _audioSources = new List<AudioSource>();

		[SerializeField] private AudioClip _gameMusic;
		[SerializeField] private AudioClip _gameMusic2;
		[SerializeField] private AudioClip _bossMusic;

		[SerializeField] private AudioSource _source;

		[SerializeField] private float delay = 50f;

		private float lastChangedTime;

		public static MusicService Instance { get; private set; }

		private void Awake()
		{
			Instance = this;

			DontDestroyOnLoad(this.gameObject);

			PlayGameMusic();

			_source.loop = true;

			lastChangedTime = 0f;
		}
		
		private AudioSource GetAvailableAudioSource()
		{
			if (_audioSources.Any(source => !source.isPlaying))
			{
				return _audioSources.First(source => !source.isPlaying);
			}
			else
			{
				return ExpandSources();
			}
		}

		private AudioSource ExpandSources()
		{
			var source = gameObject.AddComponent<AudioSource>();
			_audioSources.Add(source);

			return source;
		}

		public void PlayOtherMusic()
		{
			if (_source.clip == _gameMusic)
			{
				PlayGameMusic2();
			}
			else
			{
				PlayGameMusic();
			}
		}

		public void PlayGameMusic()
		{
			PlayMusic(_gameMusic);
		}

		public void PlayGameMusic2()
		{
			PlayMusic(_gameMusic2);
		}

		public void PlayBossMusic()
		{
			PlayMusic(_bossMusic);
		}

		public void PlayMusic(AudioClip clip)
		{
			if (_source.clip == clip)
				return;

			if (_source.isPlaying)
			{
				StopAndPlay(clip);
			}
			else
			{
				DirectPlay(clip);
			}
		}

		private void StopAndPlay(AudioClip clip)
		{
			_source.Stop();
			DirectPlay(clip);
		}

		private void DirectPlay(AudioClip clip)
		{
			_source.volume = 1.0f;
			_source.clip = clip;
			_source.Play();
		}
	}
}