﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SceneName
{
	Init = 0,
	Tutorial = 1,
	TutorialRepulsive = 2,
	TutorialAttractive = 3,
	Level0 = 4,
	Level1 = 5,
	FinalBoss = 6,
	Final = 7,
    Undefined = 50
}
