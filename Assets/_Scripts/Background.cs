﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour
{
	public Color32 MainColor;
	public GameObject player;
	public GameObject target;

	private float _hue;
	private float _saturation;
	private float _value;

	private Camera _mainCamera;

	private Vector3 _startPos;
	private float _totalDistance;

	private void Awake()
	{
		_mainCamera = Camera.main;
		if (_mainCamera == null)
		{
			Debug.LogError("No main camera found!");
		}

		Color.RGBToHSV(MainColor, out _hue, out _saturation, out _value);
	}
	
    private void Start()
    {
	    _startPos = player.transform.position;
		_totalDistance = Vector3.Distance(_startPos, target.transform.position);
    }
	
    private void Update()
    {
	    var distance = Vector3.Distance(player.transform.position, target.transform.position);
		
	    var newSaturation = Mathf.Clamp(distance / _totalDistance, 0f, 1f);

		Debug.Log("Saturation: " + newSaturation);

	    _mainCamera.backgroundColor = Color.HSVToRGB(_hue, newSaturation, _value);
    }
}
