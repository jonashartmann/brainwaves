﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Keeps track of the latest checkpoint.
 */
public class CheckpointTracker : MonoBehaviour
{
    /**
     * The last safe location
     */
    [SerializeField]
    private GameObject _Checkpoint;

    public Vector2 CheckpointLocation
    {
        get
        {
            return _Checkpoint.transform.position;
        }
    }

    public void SetCheckpoint(GameObject NewCheckpoint)
    {
        _Checkpoint = NewCheckpoint;
    }

}
