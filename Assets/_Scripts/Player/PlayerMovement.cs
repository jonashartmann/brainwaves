﻿
using UnityEngine;

/**
 * Handles all physical movement.
 */
public class PlayerMovement : MonoBehaviour
{
    public float ForceMagnitude = 5;
    public Rigidbody2D PlayerRigidBody
    {
        get
        {
            return GetComponent<Rigidbody2D>();
        }
    }

    public bool IsTouchDown { get; set;  }
    public Vector2 LastTouchLocation { get; set; }

    private void UpdateIsTouchDown()
    {
        if(Input.GetMouseButtonDown(0))
        {
            IsTouchDown = true;
        }
        else if(Input.GetMouseButtonUp(0))
        {
            IsTouchDown = false;
        }
    }

    private void UpdateTouchPosition()
    {
#if UNITY_ANDROID || UNITY_IOS
        if(Input.touchCount > 0)
        {
            LastTouchLocation = Camera.main.ScreenToWorldPoint(Input.touches[0].position);
        }
#else
        LastTouchLocation = Camera.main.ScreenToWorldPoint(Input.mousePosition);
#endif
    }

    Vector2 ComputeTouchForce()
    {
        UpdateIsTouchDown();
        UpdateTouchPosition();

        if (!IsTouchDown)
        {
            return new Vector2(0, 0);
        }

        Vector2 DeltaForce = LastTouchLocation - PlayerRigidBody.position;
        DeltaForce.Normalize();
        return DeltaForce * ForceMagnitude * Time.fixedDeltaTime;
    }

    public float MinAcceleration = 0.2f;
    public float MaxAcceleration = 1f;
    public float GyroForceMagnitude = 620f;

    Vector2 ComputeGyroscopeForce()
    {
        float accX = MathUtility.Clamp(
            Mathf.Abs(Input.acceleration.x) < MinAcceleration ? 0f : Input.acceleration.x,
            -MaxAcceleration,
            MaxAcceleration
            );
        float accY = MathUtility.Clamp(
            Mathf.Abs(Input.acceleration.y) < MinAcceleration ? 0f : Input.acceleration.y,
            -MaxAcceleration,
            MaxAcceleration
            );
        return new Vector2(accX, accY) * GyroForceMagnitude * Time.fixedDeltaTime;
    }

    /**
     * Moves the player to a location neglecting all outside forces.
     */
    public void MoveToCheckpoint(Vector2 CheckpointLocation)
    {
        transform.position = CheckpointLocation;
        
        PlayerRigidBody.velocity = new Vector2();
        PlayerRigidBody.angularVelocity = 0;
    }

    public float OppositeMovementDampening = 0.5f;

    void FixedUpdate()
    {
        Vector2 AllForces;
        if(Application.platform == RuntimePlatform.Android
            || Application.platform == RuntimePlatform.IPhonePlayer)
        {
            AllForces = ComputeGyroscopeForce();
        }
        else
        {
            AllForces = ComputeTouchForce();
        }

        float DotProduct = Vector2.Dot(PlayerRigidBody.velocity, AllForces);
        // Negative DotProduct means the angle between the two vectors is greater than 180 degrees
        if(DotProduct < 0f)
        {
            PlayerRigidBody.velocity = MathUtility.VLerp(
                PlayerRigidBody.velocity, 
                Vector2.zero,
                OppositeMovementDampening * Time.fixedDeltaTime
                );
        }
        PlayerRigidBody.AddForce(AllForces);
    }
}
