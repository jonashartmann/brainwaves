﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Enforces the game rules on the player.
 */ 
public class PlayerController : MonoBehaviour
{
    private bool _IsAlive = true;

    public GameObject LoadLevelGameObject;
    
    private void Start()
    {
        CheckpointTracker Tracker = GetComponent<CheckpointTracker>();
        transform.position = Tracker.CheckpointLocation;
    }

    public void OnPlayerDeath()
    {
        LoadLevelGameObject.GetComponent<LoadLevelScript>().PlayShowAnimation(OnFinishFadeAnimation);
    }

    private void OnFinishFadeAnimation()
    {
        CheckpointTracker Tracker = GetComponent<CheckpointTracker>();
        GetComponent<PlayerMovement>().MoveToCheckpoint(Tracker.CheckpointLocation);
        LoadLevelGameObject.GetComponent<LoadLevelScript>().PlayHideAnimation();
    }

}
