﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSmoother : MonoBehaviour
{
    public float MovementSpeed;

    void Update()
    {
        Camera.main.transform.position = new Vector3(
            Mathf.Lerp(Camera.main.transform.position.x, transform.position.x, MovementSpeed * Time.deltaTime),
            Mathf.Lerp(Camera.main.transform.position.y, transform.position.y, MovementSpeed * Time.deltaTime),
            Camera.main.transform.position.z
            );
    }
}
