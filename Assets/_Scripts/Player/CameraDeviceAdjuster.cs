﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Automatically adjusts camera orthographic resolution and position depending on the device's aspect ratio.
 */
public class CameraDeviceAdjuster : MonoBehaviour
{
    /**
     * The total intended width in pixels
     */
    public int DesignWidth = 500;
    /**
     * The number of pixels in one orthographic unit of size.
     */
    public int PixelsPerUnit = 100;
    

    void Start ()
    {
        // Recompute orthographic size
        float TargetHeight = DesignWidth * Screen.height / Screen.width;
        float TargetOrthographicSize = TargetHeight / (2 * PixelsPerUnit);
        Camera.main.orthographicSize = TargetOrthographicSize;
       

    }
	
}
